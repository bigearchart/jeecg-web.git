
#### 介绍

基于jeecg开发的小程序商城项目，代码规范遵循阿里规范，只需要替换相关参数即可使用或二开，无任何隐藏代码



#### 软件架构

软件架构说明

- 基础框架：[ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现

- JavaScript框架：Vue

- Webpack

- node

- yarn

- eslint

- @vue/cli 3.2.1

- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件

- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表

- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现

#### 使用说明

#拉取项目代码

# git clone https://gitee.com/bigearchart/jeecg-web.git

- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- Lints and fixes files
```
yarn run lint
```

 ``` 
# 1.修改前端项目的后台域名
    .env.development
    域名改成： http://jeecg-boot-system:8080/jeecg-boot
   
# 2.先进入打包前端项目
  yarn run build

# 3.构建镜像
  docker build -t nginx:jeecgboot .

# 4.启动镜像
  docker run --name jeecg-boot-nginx -p 80:80 -d nginx:jeecgboot

# 5.配置host

    # jeecgboot
    127.0.0.1   jeecg-boot-redis
    127.0.0.1   jeecg-boot-mysql
    127.0.0.1   jeecg-boot-system
  
# 6.访问前台项目
  http://localhost:80
``` 
# 具体操作可查看官方文档

# http://doc.jeecg.com/2043873


#### 参与贡献

# 如果想一起加入我们，请发送邮件或者微信联系

# 邮箱:bigearchart@163.com

# 微信:bigearchart
